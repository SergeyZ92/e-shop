import antfu from '@antfu/eslint-config'

export default antfu({
	stylistic: {
		indent: 'tab',
	},
	rules: {
		'semi': 'off',
		'antfu/if-newline': 'off',
		'curly': ['off', 'multi', 'consistent'],
		'vue/attribute-hyphenation': ['warn', 'never'],
		'vue/v-on-event-hyphenation': [
			'warn',
			'never',
			{
				autofix: true,
			},
		],
		'vue/max-attributes-per-line': [
			'warn',
			{
				singleline: { max: 1 },
				multiline: { max: 1 },
			},
		],
	},
})
