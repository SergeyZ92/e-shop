import type { RouteRecordRaw } from 'vue-router'
import { defineAsyncComponent } from 'vue'

export const routes: Array<RouteRecordRaw> = [
	{
		path: '/',
		name: 'Main',
		components: {
			default: () => import(/* webpackChunkName: "MainPage" */'./main'),
			header: () => import(/* webpackChunkName: "AppHeader" */'../widgets/app-header'),
		},
	},
	{
		path: '/catalogue',
		name: 'Catalogue',
		components: {
			default: () => import(/* webpackChunkName: "CataloguePage" */'./catalogue'),
			header: () => import(/* webpackChunkName: "AppHeader" */'../widgets/app-header'),
		},
	},
]

export const AppRouting = defineAsyncComponent(() => import('./AppRouting.vue'))
