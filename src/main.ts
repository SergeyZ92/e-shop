import '@/index.css'
import type { Router } from 'vue-router'
import type { PiniaPluginContext } from 'pinia'
import { createApp, markRaw } from 'vue'
import { AppRoot } from '@/app'
import { pinia, router } from '@/app/providers'

declare module 'pinia' {
	export interface PiniaCustomProperties {
		router: Router
	}
}

pinia.use(({ store }: PiniaPluginContext) => {
	store.router = markRaw(router)
})

createApp(AppRoot)
	.use(pinia)
	.use(router)
	.mount('#app')
